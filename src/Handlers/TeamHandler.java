package Handlers;

import Player.Player;
import Player.PlayerBasics;
import Team.Team;
import Utils.CommandLineReader;

import java.util.ArrayList;

public class TeamHandler {
	private final CommandLineReader commandLineReader;

	TeamHandler(CommandLineReader commandLineReader) {
		this.commandLineReader = commandLineReader;
	}

	public ArrayList<Player> createPlayers(int numberOfPlayers) {
		ArrayList<Player> players = new ArrayList<>();
		for (int i = 0; i < numberOfPlayers; i++) {
			System.out.println("please Enter player name");
			String name = commandLineReader.readCommandLineString();
			PlayerBasics playerBasics = new PlayerBasics(name, "", i);
			Player newPlayer = new Player(playerBasics);
			players.add(newPlayer);
		}
		return players;
	}

	public Team createTeam(int numberOfPlayers) {
		System.out.println("please Enter Team name");
		String teamName = commandLineReader.readCommandLineString();
		ArrayList<Player> players = createPlayers(numberOfPlayers);
		Team team = new Team(teamName, players);
		return team;
	}

	public Team[] createTeams(int numberOfTeams, int numberOfPlayers) {
		Team[] teams = new Team[numberOfTeams];
		for (int i = 0; i < numberOfTeams; i++) {
			Team team = createTeam(numberOfPlayers);
			teams[i] = team;
		}
		return teams;
	}
}
