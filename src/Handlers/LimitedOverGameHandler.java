package Handlers;

import Ball.Ball;
import Game.LimitedOverGame;
import Team.Team;
import Utils.CommandLineReader;

public class LimitedOverGameHandler extends GameHandler {
	CommandLineReader commandLineReader = new CommandLineReader();
	BallHandler ballHandler;
	LimitedOverGame game;

	@Override
	public void initialize() {
		System.out.println("Please input number of players");
		final int numberOfPlayers = commandLineReader.readCommandLineInt();
		TeamHandler teamHandler = new TeamHandler(commandLineReader);
		final Team[] teams = teamHandler.createTeams(2, numberOfPlayers);
		System.out.println("Please input max number of Overs in an Innings");
		final int numberOfOvers = commandLineReader.readCommandLineInt();
		final int numberOfBalls = numberOfOvers*6;

		System.out.println("Please input location of the match");
		final String location = commandLineReader.readCommandLineString();

		game = new LimitedOverGame(teams, location ,numberOfBalls);
		ballHandler = new BallHandler(commandLineReader);

	}

	@Override
	public void startGame() {
		game.startGame();
	}

	@Override
	public void playGame() {
		while (!game.canGameBeCompleted()){
			game.startNextInning();
			playInning();
		}
		game.conclude();
	}

	@Override
	public void playInning() {
		while (!game.canInningBeCompleted()) {
			game.startNextOver();
			playOver();
		}
		game.completeInnings();
	}

	@Override
	public void playOver() {
		while (!game.isOverComplete()) {
			final Ball ball = ballHandler.getBall();
			game.playBall(ball);
		}
	}


}
