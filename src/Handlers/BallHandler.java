package Handlers;

import Ball.Ball;
import Ball.ExtraBall;
import Ball.ExtraBallType;
import Ball.NormalBall;
import Ball.WicketBall;
import Utils.CommandLineReader;

public class BallHandler {

	private final CommandLineReader commandLineReader;

	BallHandler(CommandLineReader commandLineReader) {
		this.commandLineReader = commandLineReader;

	}


	public Ball getBall() {
		System.out.println("Please input type of ball. Number between 0-6/WB/NB/W");
		String ball = commandLineReader.readCommandLineString();
		return getBallFromString(ball);
		}

	private Ball getBallFromString(String ballString) {
		switch (ballString) {
			case "W": {
				//for now I've assumed 0 runs for wicket. But it's extensible
				return new WicketBall(0, 0);
			}
			case "NB": {
				return new ExtraBall(0,0, ExtraBallType.NB);
			}
			case "WB": {
				return new ExtraBall(0,0, ExtraBallType.WB);
			}
			default: {
				//Assuming correct integer value of the ball
				return new NormalBall(Integer.parseInt(ballString), 0);
			}
		}
	}
}
