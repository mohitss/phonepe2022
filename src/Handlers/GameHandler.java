package Handlers;

public abstract class GameHandler {

	public abstract void initialize();
	public abstract void startGame();
	public abstract void playGame();
	public abstract void playInning();
	public abstract void playOver();

}
