package Inning;

import Ball.Ball;
import Ball.WicketBall;
import ScoreCard.BattingScoreCard;
import ScoreCard.BowlingScoreCard;
import Team.Team;


public class Inning {
	private Team battingTeam;
	private Team bowlingTeam;

	private BattingScoreCard inningBattingScoreCard;
	private BowlingScoreCard inningBowlingScoreCard;

	private int strikePlayer;
	private int nonStrikePlayer;
	private int bowler;
	private InningState inningState;

	public Inning(Team battingTeam, Team bowlingTeam){
		this.battingTeam=battingTeam;
		this.bowlingTeam=bowlingTeam;
		this.inningBattingScoreCard = new BattingScoreCard();
		this.inningBowlingScoreCard = new BowlingScoreCard();
		this.strikePlayer=-1;
		this.nonStrikePlayer=-1;
		this.bowler=-1;
		this.inningState = InningState.NOT_STARTED;
	}



	public boolean isInningOver() {
		return inningState==InningState.COMPLETED;
	}


	public void start() {
		this.inningState = InningState.IN_PROGRESS;
		this.strikePlayer = 0;
		this.nonStrikePlayer = 1;
		this.bowler = 0;
		prepareBowling();
	}

	void prepareBowling(){
		this.inningBowlingScoreCard.prepareBowling();
		bowlingTeam.getPlayer(bowler).prepareBowling();
	}


	public boolean isOverComplete() {
		return inningBowlingScoreCard.isOverComplete();
	}

	private void battingTeamPlayBall(Ball ball){
		battingTeam.getPlayer(strikePlayer).batTheBall(ball);
		inningBattingScoreCard.addBall(ball);
		if(ball instanceof WicketBall) {
			strikePlayer = getNextBatsman();
		} else if(ball.get_player_scored_runs()%2 ==1) {
			switchStrikes();
		}
	}

	private int getNextBatsman() {
		return inningBowlingScoreCard.getWicketsTaken()+1;
	}

	private void switchStrikes() {
		int temp = strikePlayer;
		strikePlayer = nonStrikePlayer;
		nonStrikePlayer = strikePlayer;
	}

	private void bowlingTeamPlayBall(Ball ball){
		bowlingTeam.getPlayer(bowler).bowlTheBall(ball);
		inningBowlingScoreCard.addBall(ball);
	}

	public void playBall(Ball ball) {
		bowlingTeamPlayBall(ball);
		battingTeamPlayBall(ball);
	}

	public boolean areAllwicketsDown() {
		return battingTeam.getTeamSize()<=inningBowlingScoreCard.getWicketsTaken()+1;
	}

	public void completeCurrentOver() {
		getNextBowler();
		inningBowlingScoreCard.completeCurrentOver();
		switchStrikes();
	}

	private int getNextBowler() {
		return  ((bowler+1) % bowlingTeam.getTeamSize());
	}

	public boolean isBallLimitOver(int ballLimit) {
		return inningBowlingScoreCard.getTotalBalls()>=ballLimit;
	}

	public void completeCurrentInning() {
		inningState = InningState.COMPLETED;
	}
}
