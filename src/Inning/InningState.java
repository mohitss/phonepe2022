package Inning;

public enum InningState {
	NOT_STARTED, IN_PROGRESS, COMPLETED
}
