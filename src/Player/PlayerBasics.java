package Player;

public class PlayerBasics {
	//Both Name and Address classes cann be created. Stats can be added
	private String name;
	private String address;
	private int playerId;


	public PlayerBasics(String name, String address, int playerId) {
		this.name = name;
		this.address = address;
		this.playerId = playerId;
	}
}
