package Player;

import Ball.Ball;
import Ball.NormalBall;
import Ball.WicketBall;
import ScoreCard.BattingScoreCard;
import ScoreCard.BowlingScoreCard;

public class Player {
	PlayerBasics playerBasics;
	BattingScoreCard battingScoreCard;
	BowlingScoreCard bowlingScoreCard;
	BattingState battingState;

	public void batTheBall(Ball ball) {
		if(ball instanceof WicketBall) {
			battingState = BattingState.OUT;
		} else if (ball instanceof NormalBall) {
			battingScoreCard.addBall(ball);
		}
	}

	public void bowlTheBall(Ball ball) {
		bowlingScoreCard.addBall(ball);
	}

	private enum BattingState {
		TO_BAT, ON_FIELD, OUT
	}

	public Player(PlayerBasics playerBasics){
		this.playerBasics = playerBasics;
		this.battingScoreCard = new BattingScoreCard();
		this.bowlingScoreCard = new BowlingScoreCard();
		this.battingState = BattingState.TO_BAT;
	}

	public void prepareBowling() {
		bowlingScoreCard.prepareBowling();
	}





}
