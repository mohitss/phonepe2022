import Handlers.GameHandler;
import Handlers.LimitedOverGameHandler;

public class CommandController {
	public static GameHandler getHandlerForType(String gameType){
		switch (gameType) {
			case "limitedOver": return new LimitedOverGameHandler();
			default:
				throw new RuntimeException("Illegal Game Type");
		}
	}
}
