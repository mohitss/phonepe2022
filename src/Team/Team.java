package Team;

import Player.Player;

import java.util.ArrayList;

public class Team {
	private String teamName;
	private ArrayList<Player> players;

	public Team(String teamName, ArrayList<Player> players) {
		this.teamName = teamName;
		this.players = players;
	}

	public Player getPlayer(int i) {
		return players.get(i);
	}

	public int getTeamSize() {
		return players.size();
	}
}
