import Handlers.GameHandler;

public class GameController {

	public static void main(String[] args) {
		//Type of game input from the user
		GameHandler gameHandler = CommandController.getHandlerForType("limitedOver");
		gameHandler.initialize();
		gameHandler.startGame();
		gameHandler.playGame();

	}
}
