package Game;

import Ball.Ball;
import Inning.Inning;
import Team.Team;

public abstract class Game {
	private Team[] teams;
	private String location;
	private GameState gameState;
	private Team winningTeam;
	private long startTime;
	private long endTime;
	protected Inning[] innings;
	protected int currentInning;

	public Game(Team[] teams, String location) {
		this.teams = teams;
		this.location = location;
		this.gameState = GameState.NOT_STARTED;
		this.winningTeam = null;
		this.startTime = 0;
		this.endTime = 0;
	}

	public void startGame() {
		startTime = System.currentTimeMillis();
		gameState = GameState.IN_PROGRESS;
		startFirstInning();
	}

	public boolean isOverComplete() {
		return innings[currentInning].isOverComplete();
	}

	public void startNextOver() {
		innings[currentInning].completeCurrentOver();
	}

	public void completeInnings() {
		innings[currentInning].completeCurrentInning();
	}

	public abstract boolean canInningBeCompleted();

	protected void startCurrentInning() {
		this.innings[currentInning].start();
	}

	protected void startFirstInning() {
		this.currentInning = 0;
		startCurrentInning();
	}

	public void playBall(Ball ball) {
		innings[currentInning].playBall(ball);
	}

	public void startNextInning() {
		currentInning++;
	}

	public void completeGame() {
		endTime = System.currentTimeMillis();
	}

	public abstract boolean canGameBeCompleted();
	public abstract void conclude();
}
