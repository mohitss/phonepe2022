package Game;

public enum GameState {
	NOT_STARTED, IN_PROGRESS, DRAW, HALTED, COMPLETED
}
