package Game;

import Inning.Inning;
import Team.Team;

public class LimitedOverGame extends Game {
	private int ballLimit;

	public LimitedOverGame(Team[] teams, String location, int ballLimit) {
		super(teams, location);
		this.ballLimit = ballLimit;
		this.innings = new Inning[2];
		this.innings[0] = new Inning(teams[0], teams[1]);
		this.innings[1] = new Inning(teams[1], teams[0]);
	}

	@Override
	public boolean canInningBeCompleted() {
		return innings[currentInning].isBallLimitOver(ballLimit) || innings[currentInning].areAllwicketsDown();
	}

	@Override
	public void conclude() {
		super.completeGame();
		//TODO: findout winner
	}

	public boolean canGameBeCompleted() {
		return currentInning > 1;
	}


}
