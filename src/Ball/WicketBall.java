package Ball;


public class WicketBall extends Ball {

	public WicketBall(int player_scored_runs, int byes) {
		super(player_scored_runs, byes);
		this.ballType = BallType.WICKET;
	}
}
