package Ball;

public abstract class Ball {
	BallType ballType;
	int player_scored_runs;
	int byes;

	public Ball(int player_scored_runs, int byes) {
		this.player_scored_runs = player_scored_runs;
		this.byes = byes;
	}

	public int get_player_scored_runs() {
		return player_scored_runs;
	}

	public int getByes() {
		return byes;
	}

}


//	Abstract Ball - Type(EXTRA, W, R), player_scored_runs, byes, batsman, bowler
//		W - Type, bowled, Out, catchBy, runOutBy
//		E - NB, W
//		N