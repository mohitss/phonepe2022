package Ball;

public class NormalBall extends Ball {

	public NormalBall(int player_scored_runs, int byes) {
		super(player_scored_runs, byes);
		this.ballType = BallType.NORMAL;
	}

}
