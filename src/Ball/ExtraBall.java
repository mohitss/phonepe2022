package Ball;

public class ExtraBall extends Ball {
	ExtraBallType extraBallType;

	public ExtraBall(int player_scored_runs, int byes, ExtraBallType extraBallType){
		super(player_scored_runs, byes);
		this.ballType=BallType.EXTRA;
		this.extraBallType = extraBallType;
	}

}
