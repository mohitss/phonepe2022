package ScoreCard;

import Ball.Ball;
import Ball.ExtraBall;

public class BattingScoreCard {
	int sixes;
	int fours;
	int score;
	int ballsFaced;


	public BattingScoreCard() {
		this.sixes = 0;
		this.fours = 0;
		this.score = 0;
		this.ballsFaced = 0;
	}

	public void addBall(Ball ball) {
		int run = ball.get_player_scored_runs();
		this.score += run;
		if(!(ball instanceof ExtraBall))
			this.ballsFaced++;
		if(run == 4)
			fours++;
		else if(run==6)
			sixes++;
	}
}
