package ScoreCard;

import Ball.Ball;
import Ball.ExtraBall;
import Ball.WicketBall;

public class BowlingScoreCard {
	int runsGiven;
	int extrasGiven;
	int balls;
	int currentOver;
	int wicketsTaken;

	public BowlingScoreCard() {
		this.runsGiven = 0;
		this.extrasGiven = 0;
		this.balls = 0;
		this.currentOver=0;
		this.wicketsTaken = 0;
	}

	public int getWicketsTaken() {
		return wicketsTaken;
	}

	public int getTotalBalls() {
		return balls;
	}

	public void prepareBowling(){
		this.currentOver=1;
	}

	public boolean isOverComplete() {
		return balls/6==currentOver;
	}

	public void addBall(Ball ball) {
		int player_runs = ball.get_player_scored_runs();
		int byes = ball.getByes();
		int extra = ball instanceof ExtraBall ? 1 : 0;
		int wicketTaken = ball instanceof WicketBall? 1:0;
		this.runsGiven += player_runs + byes + extra;
		this.extrasGiven += extra + byes;
		this.balls += 1-extra;
		this.wicketsTaken += 1+wicketTaken;
	}

	public void completeCurrentOver() {
		this.currentOver++;
	}
}