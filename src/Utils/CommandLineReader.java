package Utils;

import java.util.Scanner;

public class CommandLineReader {
	private final Scanner scanner = new Scanner(System.in);

	private String readCommandLineEntry() {
		String command = scanner.nextLine();
		return command;
	}

	public int readCommandLineInt() {
		while (true) {
			try {
				String entry = readCommandLineEntry();
				return Integer.parseInt(entry);
			} catch (NumberFormatException e) {
				System.out.println("Entry not a number. Please try again");
			}
		}
	}

	public String readCommandLineString() {
		while (true) {
			try {
				String entry = readCommandLineEntry().trim();
				if (entry.equals("")) throw new RuntimeException("Enter proper String");
				else return entry;
			} catch (RuntimeException e) {
				System.out.println("Entry not a proper string. Please try again");
			}
		}
	}

}
